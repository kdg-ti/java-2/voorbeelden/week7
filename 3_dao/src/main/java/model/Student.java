package model;

import java.time.LocalDate;

public class Student {
    private int id; //PK

    private final int studNr;
    private final String naam;
    private final LocalDate geboorte;
    private String woonplaats;

    public Student(int id, int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.id = id;
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }
    // Constructor zonder id:
    public Student(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this(-1, studNr, naam, geboorte, woonplaats);
    }

    public int getId() {
        return id;
    }

    public int getStudNr() {
        return studNr;
    }

    public String getNaam() {
        return naam;
    }

    public LocalDate getGeboorte() {
        return geboorte;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}