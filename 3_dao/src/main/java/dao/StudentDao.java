package dao;

import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO klasse die de communicatie met de database doet.
 * Voor de eenvoud werd deze klasse heel summier uitgewerkt
 * Uiteraard moeten nog andere CRUD-methoden voorzien worden.
 */
public class StudentDao {
    private Connection connection;

    public StudentDao() {
        maakConnectie();
        maakTabel();
    }

    private void maakConnectie() {
        String databasePath = "db/myDatabase";
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath, "sa", "");
            System.out.println("Connection gemaakt");
        } catch (SQLException e) {
            System.err.println("Kan geen connectie maken met database " + databasePath);
            System.exit(1);
        }
    }

    private void maakTabel() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS studentendb");
            String createQuery = "CREATE TABLE studentendb " +
                    "(id INTEGER NOT NULL IDENTITY," +
                    "studnr INTEGER NOT NULL, " +
                    "naam VARCHAR(30) NOT NULL," +
                    "geboorte DATE," +
                    "woonplaats VARCHAR(40))";
            statement.execute(createQuery);
            System.out.println("Database aangemaakt");
        } catch (SQLException e) {
            String message = e.getMessage();
            if (message.contains("Tabel bestaat al")) return;
            System.err.println("Onverwachte fout bij aanmaken tabel: " + e.getMessage());
            System.exit(1);
        }
    }

    public void close() {
        if (connection == null) return;
        try {
            Statement statement = connection.createStatement();
            statement.execute("SHUTDOWN COMPACT");
            statement.close();
            connection.close();
            System.out.println("\nDatabase gesloten");
        } catch (SQLException e) {
            System.out.println("Probleem bij sluiten van database: " + e.getMessage());
        }
    }

    public boolean voegToe(Student student) {
        if (student.getId() >= 0) return false; //student heeft al PK dus bestaat al in database
        try {
            PreparedStatement prepStatement = connection.prepareStatement(
                    "INSERT INTO studentendb (id, studnr, naam, geboorte, woonplaats) " +
                            "VALUES (NULL, ?, ?, ?, ?)");
            prepStatement.setInt(1, student.getStudNr());
            prepStatement.setString(2, student.getNaam());
            prepStatement.setDate(3, Date.valueOf(student.getGeboorte()));
            prepStatement.setString(4, student.getWoonplaats());

            boolean result = prepStatement.executeUpdate() == 1;
            prepStatement.close();
            return result;

        } catch (SQLException e) {
            System.err.println("Fout bij create: " + e);
            return false;
        }
    }

    public List<Student> getAllRecords() {
        ResultSet rs = null;
        List<Student> myList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("SELECT * FROM studentendb ORDER BY studnr");
            while (rs.next()) {
                myList.add(new Student(
                        rs.getInt("id"),
                        rs.getInt("studnr"),
                        rs.getString("naam"),
                        rs.getDate("geboorte").toLocalDate(),
                        rs.getString("woonplaats")
                ));
            }
        } catch (SQLException e) {
            System.err.println("Fout bij create: " + e);
        }
        return myList;
    }
}

