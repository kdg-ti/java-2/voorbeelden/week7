import dao.StudentDao;
import model.Student;

import java.time.LocalDate;

public class DemoDao {
    public static void main(String[] args) {
        StudentDao dao = new StudentDao();
        dao.voegToe(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        dao.voegToe(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        dao.voegToe(new Student(123, "Sam Gooris", LocalDate.of(1973, 4, 10), "Antwerpen"));
        dao.voegToe(new Student(333, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));

        System.out.println("Na aanmaken en vullen van databank:");
        dao.getAllRecords().forEach(System.out::println);
        dao.close();

    }
}
