package model;

import java.io.Serializable;
import java.time.LocalDate;

public class Student {
    private int id; //PK

    private final int studNr;
    private final String naam;
    private final LocalDate geboorte;
    private String woonplaats;

    public Student(int id, int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
        this.id = id;
    }

    public Student(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this(-1, studNr, naam, geboorte, woonplaats);
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}