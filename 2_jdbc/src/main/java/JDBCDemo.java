import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCDemo {
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            // STAP 1. Maak een connection met de database
            connection = DriverManager.getConnection("jdbc:hsqldb:file:db/studenten", "sa", "");

            // STAP 2. Maak een statement klaar
            statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS studenten");
            String createQuery = "CREATE TABLE studenten " +
                    "(id INTEGER NOT NULL IDENTITY," +
                    "studnr INTEGER NOT NULL, " +
                    "naam VARCHAR(30) NOT NULL," +
                    "geboorte DATE," +
                    "woonplaats VARCHAR(40))";
            //System.out.println(createQuery);
            statement.execute(createQuery);

            // STAP3. Execute query
            String query1 = "INSERT INTO studenten VALUES (NULL, 111, 'Kasper', '"
                    + Date.valueOf("2000-8-15") + "', 'Berchem')";
            int rowsAffected = statement.executeUpdate(query1);
            String query2 = "INSERT INTO studenten VALUES (NULL, 222, 'Melchior', '"
                    + Date.valueOf("1999-10-19") + "', 'Antwerpen')";
            rowsAffected += statement.executeUpdate(query2);
            String query3 = "INSERT INTO studenten VALUES (NULL, 333, 'Balthazar', '"
                    + Date.valueOf("2001-3-2") + "', 'Kortrijk')";
            rowsAffected += statement.executeUpdate(query3);
            System.out.println("rowsAffected: " + rowsAffected);

            // STAP 4. Verwerk de opgehaalde data
            List<Student> myList = new ArrayList<>();
            resultSet = statement.executeQuery("SELECT * FROM studenten");
            while (resultSet.next()) {
                Student student = new Student(
                        resultSet.getInt("id"),
                        resultSet.getInt("studnr"),
                        resultSet.getString("naam"),
                        resultSet.getDate("geboorte").toLocalDate(),
                        resultSet.getString("woonplaats"));
                myList.add(student);
            }
            System.out.println("opgehaalde data:");
            myList.forEach(System.out::println);

            // STAP 5. Verbreek de verbinding met de database
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState = " + e.getSQLState());
            System.out.println("Error code = " + e.getErrorCode());
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


