package model;

import java.io.Serializable;
import java.time.LocalDate;

public class Student implements Serializable{
    private final int studNr;
    private final String naam;
    private final LocalDate geboorte;
    private transient String woonplaats;
    private static final long serialVersionUID = 1L;

    public Student(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }

    public Student(int studNr, String naam, LocalDate geboorte) {
        this(studNr, naam, geboorte, "");
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}
