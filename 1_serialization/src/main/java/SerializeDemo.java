import model.Student;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SerializeDemo {
	public static final String SERIAL_FILE="data/studenten.ser";
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        studentList.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        studentList.add(new Student(123, "Sam Gooris", LocalDate.of(1973, 4, 10), "Antwerpen"));
        studentList.add(new Student(333, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));

        System.out.println("Voor serialize:");
        studentList.forEach(System.out::println);

        try (ObjectOutputStream oos = new ObjectOutputStream(
	        new FileOutputStream(SERIAL_FILE))) {
	        oos.writeObject(studentList);
	        studentList.clear();
	        System.out.println("\nNa serialize / deserialize: (sommige data staan op null omwille van transient)");
        } catch (IOException e) {
	        e.printStackTrace();
        }

	    try (    ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(SERIAL_FILE))){
            Object object = ois.readObject();
            studentList = (List<Student>)object;
            studentList.forEach(System.out::println);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
